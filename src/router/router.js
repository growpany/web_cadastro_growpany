import Router from 'vue-router'
import Vue from 'vue'

import Main from '@/modules/main'
import Login from '@/modules/auth'
import LoginOrRegister from '@/modules/main/modules/inicio/LoginOrRegister'
import Register from '@/modules/register'
// import Perfil from '@/modules/user'
import CompanyRoute from '@/modules/company/router'
import UserRoute from '@/modules/user/router'
import CustomerRoute from '@/modules/customer/router'
import Home from '@/modules/home'

Vue.use(Router)
export default new Router({
  mode: 'hash',
  hashbang: false,
  routes: [
    // Ponto onde sempre será redirecionado se uma url for digitar incorretamente
    {
      path: '*',
      component: Home,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem('grw-token')) {
          next('/')
        } else {
          next('/account')
        }
      }
    },
    // Account
    {
      path: '/account',
      component: Main,
      children: [
        {
          path: '/',
          name: 'login-register',
          component: LoginOrRegister
        },
        {
          path: '/login',
          name: 'Login',
          component: Login
        },
        {
          path: '/register',
          name: 'Register',
          component: Register
        }
      ],
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem('grw-token')) {
          next('*')
        } else {
          next()
        }
      }
    },
    // Home
    {
      path: '/',
      component: Home,
      beforeEnter: (to, from, next) => {
        if (!localStorage.getItem('grw-token')) {
          next('/account')
        } else {
          next()
        }
      },
      children: [...UserRoute, ...CompanyRoute, ...CustomerRoute]
    }
  ]
})
