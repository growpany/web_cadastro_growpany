import MainModule from '@/modules/main/store'
import HomeModule from '@/modules/home/store'
import AuthModule from '@/modules/auth/store'
import UserModule from '@/modules/user/store'
import RegisterModule from '@/modules/register/store'
import CompaniesModule from '@/modules/company/companies/store'
import CompanyModule from '@/modules/company/store'
import CompanyCreateModule from '@/modules/company/create/store'
import EditCompanyModule from '@/modules/company/edit/store'
import CustomersModule from '@/modules/customer/customers/store'
import CustomerDiagnosticsModule from '@/modules/customer/diagnostic/store'

export default {
  MainModule,
  HomeModule,
  AuthModule,
  UserModule,
  RegisterModule,
  CompaniesModule,
  CompanyModule,
  CompanyCreateModule,
  EditCompanyModule,
  CustomersModule,
  CustomerDiagnosticsModule
}
