const getDefaultState = () => {
  return {
    confirmDelete: false,
    companies: Array
  }
}

export { getDefaultState }
export default {
  confirmDelete: false,
  companies: Array
}
