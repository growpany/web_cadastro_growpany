import apiGrowpany from '@/apiGrowpany'
export default {
  getAllCompanies: ({ commit }) => {
    const token = localStorage.getItem('grw-token')
    if (token) {
      fetch(`${apiGrowpany.baseURL}/empresa/minhas?token=${token}`)
        .then(response => {
          return response.json()
        })
        .then(jsonData => {
          if (!jsonData.error) {
            commit('setCompanies', jsonData.data)
          } else {
            throw new Error(jsonData.msg)
          }
        })
        .catch(jsonData => {
          return null
        })
    }
  },
  deleteCompany: ({ commit, state }, index) => {
    const company = state.companies[index]
    const token = localStorage.getItem('grw-token')
    if (token) {
      fetch(`${apiGrowpany.baseURL}/empresa/deletar`, {
        method: 'post',
        body: JSON.stringify({ token: token, id: company.id })
      })
        .then(response => {
          return response.json()
        })
        .then(jsonData => {
          if (!jsonData.error) {
            commit('deleteCompany', index)
          } else {
            throw new Error(jsonData.msg)
          }
        })
        .catch(e => {
          console.log(e.message)
        })
    }
  },
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
