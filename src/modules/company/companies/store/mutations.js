import { getDefaultState } from './state'

export default {
  setCompanies: (state, companies) => {
    companies.forEach(company => {
      company.id = parseInt(company.id)
      company.cod_segmento = parseInt(company.cod_segmento)
    })
    state.companies = companies
  },
  deleteCompany: (state, index) => {
    state.companies.splice(index, 1)
  },
  resetState: state => {
    Object.assign(state, getDefaultState())
  }
}
