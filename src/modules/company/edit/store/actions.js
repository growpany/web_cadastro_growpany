import apiGrowpany from '@/apiGrowpany'
export default {
  getInfo: ({ commit, state }, idCompany) => {
    state.idCompany = idCompany
    const token = localStorage.getItem('grw-token')
    return fetch(
      `${apiGrowpany.baseURL}/empresa/info?token=${token}&id=${idCompany}`
    )
      .then(response => {
        return response.json()
      })
      .then(jsonData => {
        if (jsonData.error) {
          throw new Error('Nenhuma informação recuperada')
        }
        commit('setCompany', jsonData.data)
      })
      .catch(() => {
        throw new Error('Nenhuma informação recuperada')
      })
  },
  onNameChanged: ({ commit }, value) => {
    commit(
      'onNameChanged',
      value.target.value === '' ? null : value.target.value
    )
  },
  onPhoneChanged: ({ commit }, value) => {
    commit(
      'onPhoneChanged',
      value.target.value === '' ? null : value.target.value
    )
  },
  onDescriptionChanged: ({ commit }, value) => {
    commit(
      'onDescriptionChanged',
      value.target.value === '' ? null : value.target.value
    )
  },
  onCEPChanged: ({ commit }, value) => {
    commit(
      'onCEPChanged',
      value.target.value === '' ? null : value.target.value.replace(/\.|-/g, '')
    )
  },
  onSegmentChanged: ({ commit }, value) => {
    commit('onSegmentChanged', value)
  },
  onLogoChanged: ({ commit }, value) => {
    commit(
      'onLogoChanged',
      value.target.value === '' ? null : value.target.value
    )
  },
  handleFileUpload: ({ commit }, event) => {
    const file = event.target.files[0]
    if (file) {
      if (file.size <= 524288) {
        var reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = function() {
          commit(
            'onImageChanged',
            reader.result.substr(reader.result.indexOf(',') + 1)
          )
        }
        reader.onerror = function(error) {
          console.log('Error: ', error)
        }
        commit('saveUrlImage', URL.createObjectURL(file))
      }
    }
  },
  // Action Login
  update: ({ commit, state }) => {
    const body = {
      id: state.idCompany,
      token: localStorage.getItem('grw-token'),
      nome: state.form.nome,
      telefone: state.form.telefone,
      descricao: state.form.descricao,
      logo: state.form.logo,
      cep: state.form.cep,
      cod_segmento: state.form.cod_segmento
    }
    console.log(body)
    state.isLoading = true
    fetch(`${apiGrowpany.baseURL}/empresa/alterar`, {
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(result => {
        return result.json()
      })
      .then(jsonData => {
        if (!jsonData.error) {
          state.isLoading = false
          commit('CompaniesModule/navToPageName', 'companies', { root: true })
        } else {
          throw new Error(jsonData.msg)
        }
      })
      .catch(_ => {
        state.isLoading = false
      })
  },
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
