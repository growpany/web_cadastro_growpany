const getDefaultState = () => {
  return {
    isLoading: false,
    urlImage: null,
    idCompany: null,
    empresa: {
      nome: null,
      telefone: null,
      descricao: null,
      logo: null,
      cep: null,
      cod_segmento: 2
    },
    form: {
      nome: null,
      telefone: null,
      descricao: null,
      logo: null,
      cep: null,
      cod_segmento: null
    }
  }
}
export { getDefaultState }

export default {
  isLoading: false,
  urlImage: null,
  idCompany: null,
  empresa: {
    nome: null,
    telefone: null,
    descricao: null,
    logo: null,
    cep: null,
    cod_segmento: 2
  },
  form: {
    nome: null,
    telefone: null,
    descricao: null,
    logo: null,
    cep: null,
    cod_segmento: 2
  }
}
