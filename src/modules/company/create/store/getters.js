export default {
  getAllSegments: () => {
    return [
      {
        id: '2',
        nome: 'Alimentação'
      },
      {
        id: '3',
        nome: 'Indústria'
      },
      {
        id: '4',
        nome: 'Serviço'
      },
      {
        id: '5',
        nome: 'Varejo'
      },
      {
        id: '6',
        nome: 'Empreendedor'
      }
    ]
  }
}
