import { getDefaultState } from './state'

export default {
  onNameChanged: (state, nome) => {
    state.form.nome = nome
  },
  onPhoneChanged: (state, phone) => {
    state.form.telefone = phone
  },
  onDescriptionChanged: (state, description) => {
    state.form.descricao = description
  },
  onCEPChanged: (state, cep) => {
    state.form.cep = cep
  },
  onLogoChanged: (state, logo) => {
    state.form.logo = logo
  },
  onSegmentChanged: (state, value) => {
    state.form.cod_segmento = value
  },
  saveUrlImage: (state, url) => {
    state.urlImage = url
  },
  // inputs changes
  onImageChanged: (state, base64) => {
    state.form.logo = base64
  },
  resetState: (state) => {
    Object.assign(state, getDefaultState())
  }
}
