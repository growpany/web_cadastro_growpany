import apiGrowpany from '@/apiGrowpany'
export default {
  onNameChanged: ({ commit }, value) => {
    commit('onNameChanged', value.target.value)
  },
  onPhoneChanged: ({ commit }, value) => {
    commit('onPhoneChanged', value.target.value)
  },
  onDescriptionChanged: ({ commit }, value) => {
    commit('onDescriptionChanged', value.target.value)
  },
  onCEPChanged: ({ commit }, value) => {
    commit('onCEPChanged', value.target.value.replace(/\.|-/g, ''))
  },
  onSegmentChanged: ({ commit }, value) => {
    commit('onSegmentChanged', value)
  },
  onLogoChanged: ({ commit }, value) => {
    commit('onLogoChanged', value.target.value)
  },
  handleFileUpload: ({ commit }, event) => {
    const file = event.target.files[0]
    if (file) {
      if (file.size <= 524288) {
        var reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = function() {
          commit(
            'onImageChanged',
            reader.result.substr(reader.result.indexOf(',') + 1)
          )
        }
        reader.onerror = function(error) {
          console.log('Error: ', error)
        }
        commit('saveUrlImage', URL.createObjectURL(file))
      }
    }
  },
  // Action Login
  save: async({ commit, state }) => {
    const body = {
      token: localStorage.getItem('grw-token'),
      nome: state.form.nome,
      telefone: state.form.telefone,
      descricao: state.form.descricao,
      logo: state.form.logo,
      cep: state.form.cep,
      cod_segmento: state.form.cod_segmento
    }
    state.isLoading = true
    fetch(`${apiGrowpany.baseURL}/empresa/cadastrar`, {
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(result => {
        return result.json()
      })
      .then(jsonData => {
        if (!jsonData.error) {
          state.isLoading = false
        } else {
          throw new Error(jsonData.msg)
        }
      })
      .catch(_ => {
        state.isLoading = false
      })
  },
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
