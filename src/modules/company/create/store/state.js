const getDefaultState = () => {
  return {
    isLoading: false,
    urlImage: null,
    form: {
      nome: null,
      telefone: null,
      descricao: null,
      logo: null,
      cep: null,
      cod_segmento: 2
    }
  }
}
export { getDefaultState }

export default {
  isLoading: false,
  urlImage: null,
  form: {
    nome: null,
    telefone: null,
    descricao: null,
    logo: null,
    cep: null,
    cod_segmento: 2
  }
}
