const getDefaultState = () => {
  return {
    customers: []
  }
}

export { getDefaultState }
export default {
  customers: []
}
