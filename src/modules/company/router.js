import Companies from './companies'
import Edit from './edit'
import Create from './create'
// import Info from './teste'

export default [
  {
    path: '/companies',
    name: 'companies',
    component: Companies
  },
  {
    path: '/company/info/:id',
    name: 'company.info',
    component: Edit
  },
  {
    path: '/company/create',
    name: 'company.create',
    component: Create
  },
  {
    path: '/company/edit/:id',
    name: 'company.edit',
    component: Edit
  }
]
