import { getDefaultState } from './state'

export default {
  setCustomers: (state, customers) => {
    customers.forEach(customer => {
      customer.id_cliente = parseInt(customer.id_cliente)
    })
    state.customers = customers
  },
  resetState: state => {
    Object.assign(state, getDefaultState())
  }
}
