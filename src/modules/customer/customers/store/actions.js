import apiGrowpany from '@/apiGrowpany'

export default {
  getMyCustomers({ commit }) {
    const token = localStorage.getItem('grw-token')
    if (token) {
      fetch(`${apiGrowpany.baseURL}/consultoria/clientes?token=${token}`)
        .then(response => {
          return response.json()
        })
        .then(jsonData => {
          if (!jsonData.error) {
            commit('setCustomers', jsonData.data)
          } else {
            throw new Error(jsonData.msg)
          }
        })
        .catch(_ => {
          return null
        })
    }
  },
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
