const getDefaultState = () => {
  return {
    pageRouteName: 'diagnostics',
    confirmDelete: false,
    companies: []
  }
}

export { getDefaultState }
export default {
  pageRouteName: 'diagnostics',
  confirmDelete: false,
  companies: []
}
