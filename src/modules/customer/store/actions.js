import apiGrowpany from '@/apiGrowpany'
// Listar
// inserir
// deletar
// atualizar
export default {
  getAllCompanies: ({ commit }) => {
    const token = localStorage.getItem('grw-token')
    if (token) {
      fetch(`${apiGrowpany.baseURL}/empresa/minhas?token=${token}`)
        .then((response) => {
          return response.json()
        })
        .then((jsonData) => {
          if (!jsonData.error) {
            commit('setCompanies', jsonData.data)
          } else {
            throw new Error(jsonData.msg)
          }
        })
        .catch((e) => {
          console.log(e.message)
        })
    }
  },
  deleteCompany: ({ commit, state }, index) => {
    const company = state.companies[index]
    const token = localStorage.getItem('grw-token')
    if (token) {
      fetch(`${apiGrowpany.baseURL}/empresa/deletar`, {
        method: 'post',
        body: JSON.stringify({ token: token, id: company.id })
      })
        .then((response) => {
          return response.json()
        })
        .then((jsonData) => {
          console.log(jsonData)
          if (!jsonData.error) {
            commit('deleteCompany', index)
          } else {
            throw new Error(jsonData.msg)
          }
        })
        .catch((e) => {
          console.log(e.message)
        })
    }
  },
  navToPageName: ({ commit }, pageName) => {
    commit('navToPageName', pageName)
  },
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
