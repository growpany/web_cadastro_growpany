import { getDefaultState } from './state'

export default {
  setDiagnostics: (state, diagnostics) => {
    diagnostics.forEach(diagnostic => {
      diagnostic.id = parseInt(diagnostic.id)
      diagnostic.fk_diagnosticacao = parseInt(diagnostic.fk_diagnosticacao)
    })
    state.diagnostics = diagnostics
  },
  setCustomer: (state, customer) => {
    state.customer = customer
  },
  resetState: state => {
    Object.assign(state, getDefaultState())
  }
}
