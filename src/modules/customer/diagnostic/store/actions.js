import apiGrowpany from '@/apiGrowpany'

export default {
  getDiagnostics: ({ commit }, payload) => {
    const token = localStorage.getItem('grw-token')
    if (token) {
      fetch(
        `${apiGrowpany.baseURL}/consultoria/respostas_diagnosticos/?token=${token}&id_empresa=${payload.id}`
      )
        .then(response => {
          return response.json()
        })
        .then(jsonData => {
          if (!jsonData.error) {
            commit('setDiagnostics', jsonData.data)
          } else {
            throw new Error(jsonData.msg)
          }
        })
        .catch(_ => {
          return null
        })
    }
  },
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
