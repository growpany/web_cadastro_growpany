const getDefaultState = () => {
  return {
    diagnostics: Array,
    customer: String
  }
}

export default {
  diagnostics: Array,
  customer: String
}

export { getDefaultState }
