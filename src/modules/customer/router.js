import Customers from './customers'
import CustomerDiagnostics from './diagnostic'

export default [
  {
    path: '/customers',
    name: 'customers',
    component: Customers
  },
  {
    path: '/customer/diagnostics/:id',
    name: 'customer.diagnostics',
    component: CustomerDiagnostics
  }
]
