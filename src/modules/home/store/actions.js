export default {
  navToPageName: ({ commit }, pageName) => {
    commit('navToPageNamed', pageName)
    if (pageName === 'user') {
      commit('UserModule/changeEditingState', false, { root: true })
    }
    if (pageName === 'companies') {
      commit('CompaniesModule/navToPageName', 'companies', { root: true })
    }
    if (pageName === 'diagnostics') {
      commit('DiagnosticsModule/navToPageName', 'diagnostics', { root: true })
    }
  }
}
