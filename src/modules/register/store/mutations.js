import { getDefaultState } from './state'
export default {
  nextForm: (state) => {
    state.formIndex++
  },
  previewForm: (state) => {
    state.formIndex--
  },
  saveUrlImage: (state, url) => {
    state.urlImage = url
  },
  // inputs changes
  onImageChanged: (state, base64) => {
    state.form.foto = base64
  },
  onNameChanged: (state, value) => {
    state.form.nome = value
  },
  onSurnameChanged: (state, value) => {
    state.form.sobrenome = value
  },
  onEmailChanged: (state, value) => {
    state.form.email = value
  },
  onPhoneChanged: (state, value) => {
    state.form.telefone = value
  },
  onCellPhoneChanged: (state, value) => {
    state.form.celular = value
  },
  onPasswordChanged: (state, value) => {
    state.form.senha = value
  },
  onPasswordConfirmChanged: (state, value) => {
    state.form.confirmPass = value
  },
  // Reset
  resetState: (state) => {
    Object.assign(state, getDefaultState())
  }
}
