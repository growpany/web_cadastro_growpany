import apiGrowpany from '@/apiGrowpany'
export default {
  nextForm: ({ commit }) => {
    commit('nextForm')
  },
  previewForm: ({ commit }) => {
    commit('previewForm')
  },
  handleFileUpload: ({ commit }, event) => {
    const file = event.target.files[0]
    if (file) {
      if (file.size <= 524288) {
        var reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = function() {
          commit(
            'onImageChanged',
            reader.result.substr(reader.result.indexOf(',') + 1)
          )
        }
        reader.onerror = function(error) {
          console.log('Error: ', error)
        }
        commit('saveUrlImage', URL.createObjectURL(file))
      }
    }
  },
  // inputs change
  generateUrlImage: ({ commit }, url) => {
    commit('generateUrlImage', url)
  },
  onNameChanged: ({ commit }, value) => {
    commit('onNameChanged', value.target.value)
  },
  onSurnameChanged: ({ commit }, value) => {
    commit('onSurnameChanged', value.target.value)
  },
  onEmailChanged: ({ commit }, value) => {
    commit('onEmailChanged', value.target.value)
  },
  onCellPhoneChanged: ({ commit }, value) => {
    commit('onCellPhoneChanged', value.target.value)
  },
  onPhoneChanged: ({ commit }, value) => {
    commit('onPhoneChanged', value.target.value)
  },
  onPasswordChanged: ({ commit }, value) => {
    commit('onPasswordChanged', value.target.value)
  },
  onPasswordConfirmChanged: ({ commit }, value) => {
    commit('onPasswordConfirmChanged', value.target.value)
  },
  // Action Login
  save: ({ commit, state, getters }) => {
    const body = {
      token: state.form.token,
      foto: state.form.foto,
      nome: state.form.nome,
      sobrenome: state.form.sobrenome,
      email: state.form.email,
      telefone: state.form.telefone,
      celular: state.form.celular,
      senha: getters.md5Password
    }
    state.isLoading = true
    return fetch(`${apiGrowpany.baseURL}/usuario/cadastrar`, {
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(result => {
        return result.json()
      })
      .then(jsonData => {
        if (!jsonData.error) {
          state.isLoading = false
          return true
        } else {
          throw new Error(jsonData.msg)
        }
      })
      .catch(_ => {
        return false
      })
  },
  // Reset state
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
