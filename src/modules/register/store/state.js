const getDefaultState = () => {
  return {
    formIndex: 0,
    isLoading: false,
    urlImage: '',
    form: {
      token: 'app:growpany',
      foto: '',
      nome: '',
      sobrenome: '',
      email: '',
      telefone: '',
      celular: '',
      senha: '',
      confirmPass: ''
    }
  }
}
export { getDefaultState }
export default {
  formIndex: 0,
  isLoading: false,
  urlImage: '',
  form: {
    token: 'app:growpany',
    foto: '',
    nome: '',
    sobrenome: '',
    email: '',
    telefone: '',
    celular: '',
    senha: '',
    confirmPass: ''
  }
}
