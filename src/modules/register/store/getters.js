import md5 from 'crypto-js/md5'

export default {
  isValidForm: (state) => {
    if (state.formIndex === 0) {
      return !(
        !state.form.nome.length ||
          !state.form.sobrenome.length ||
          !state.form.email.length
      )
    }
    if (state.formIndex === 1) {
      return !(!state.form.telefone.length || !state.form.celular.length)
    }
    if (state.formIndex === 2) {
      return !(!state.form.senha.length || !state.form.confirmPass.length)
    }
    return true
  },
  isMatchPassword: (state) => {
    return state.form.senha === state.form.confirmPass
  },
  formIndex: (state) => {
    return state.formIndex
  },
  md5Password: (state) => {
    if (state.form.senha !== '') {
      return md5(state.form.senha).toString()
    }
    return null
  }
}
