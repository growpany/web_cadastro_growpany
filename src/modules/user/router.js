import Info from './info'
import Edit from './edit'

export default [
  {
    path: '',
    name: 'userboard',
    component: Info
  },
  {
    path: '/profile',
    name: 'profile.info',
    component: Info
  },
  {
    path: '/profile/edit',
    name: 'profile.edit',
    component: Edit
  }
]
