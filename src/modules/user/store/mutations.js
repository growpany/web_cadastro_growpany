import { getDefaultState, getDefaultFormState } from './state'
export default {
  onTokenChanged: (state, token) => {
    if (token) {
      localStorage.setItem('grw-token', token)
      state.hasTokenAuth = true
    } else {
      localStorage.removeItem('grw-token')
      state.hasTokenAuth = false
    }
  },
  changeLoadingState: (state, isLoading) => {
    state.isLoading = isLoading
  },
  setUser: (state, user) => {
    localStorage.setItem('grw-token', user.token)
    state.user.id = parseInt(user.id)
    state.user.nome = user.nome
    state.user.sobrenome = user.sobrenome
    state.user.email = user.email
    state.user.telefone = user.telefone
    state.user.celular = user.celular
    state.user.foto = user.foto
    state.hasTokenAuth = true
  },
  saveUrlImage: (state, url) => {
    state.formEdit.urlImage = url
  },
  // inputs changes
  onImageChanged: (state, base64) => {
    state.formEdit.foto = base64
  },
  onNameChanged: (state, value) => {
    state.formEdit.nome = value
  },
  onSurnameChanged: (state, value) => {
    state.formEdit.sobrenome = value
  },
  onEmailChanged: (state, value) => {
    state.formEdit.email = value
  },
  onPhoneChanged: (state, value) => {
    state.formEdit.telefone = value
  },
  onCellPhoneChanged: (state, value) => {
    state.formEdit.celular = value
  },
  setIsMatchOldPass: (state, value) => {
    state.isMatchOldPass = value
  },
  onOldPasswordChanged: (state, value) => {
    state.formEdit.oldPass = value
  },
  onNewPasswordChanged: (state, value) => {
    state.formEdit.newPass = value
  },
  // Reset state
  resetState: state => {
    Object.assign(state, getDefaultState())
  },
  resetFormState: state => {
    Object.assign(state, getDefaultFormState())
  }
}
