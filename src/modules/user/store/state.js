const getDefaultState = () => {
  return {
    isLoading: false,
    isEditing: false,
    hasTokenAuth: false,
    isMatchOldPass: false,
    user: {
      id: null,
      nome: null,
      sobrenome: null,
      email: null,
      telefone: null,
      celular: null,
      foto: null,
      senha: null
    },
    formEdit: {
      urlImage: null,
      isLoading: false,
      token: 'app:growpany',
      foto: null,
      nome: null,
      sobrenome: null,
      email: null,
      telefone: null,
      celular: null,
      oldPass: null,
      newPass: null
    }
  }
}
const getDefaultFormState = () => {
  return {
    formEdit: {
      urlImage: null,
      isLoading: false,
      token: 'app:growpany',
      foto: null,
      nome: null,
      sobrenome: null,
      email: null,
      telefone: null,
      celular: null,
      oldPass: null,
      newPass: null
    }
  }
}
export { getDefaultState, getDefaultFormState }
export default {
  isLoading: false,
  isEditing: false,
  hasTokenAuth: false,
  isMatchOldPass: false,
  user: {
    id: null,
    nome: null,
    sobrenome: null,
    email: null,
    telefone: null,
    celular: null,
    foto: null,
    senha: null
  },
  formEdit: {
    urlImage: null,
    isLoading: false,
    token: 'app:growpany',
    foto: null,
    nome: null,
    sobrenome: null,
    email: null,
    telefone: null,
    celular: null,
    oldPass: null,
    newPass: null
  }
}
