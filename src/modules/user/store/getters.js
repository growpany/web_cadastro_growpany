import md5 from 'crypto-js/md5'

export default {
  getUser: state => state.user,
  hasTokenAuth: state => {
    if (localStorage.getItem('grw-token')) {
      state.hasTokenAuth = true
      return state.hasTokenAuth
    }
    state.hasTokenAuth = false
    return state.hasTokenAuth
  },
  md5OldPassword: state => {
    if (state.formEdit.oldPass !== '') {
      return md5(state.formEdit.oldPass).toString()
    }
    return null
  },
  md5NewPassword: state => {
    if (state.formEdit.newPass !== '') {
      return md5(state.formEdit.newPass).toString()
    }
    return null
  }
}
