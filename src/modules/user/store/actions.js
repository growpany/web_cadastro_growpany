import apiGrowpany from '@/apiGrowpany'
export default {
  setUser({ commit }, user) {
    commit('setUser', user)
    commit('onTokenChanged', user.token)
  },
  onTokenChanged: ({ commit }, token) => {
    commit('onTokenChanged', token)
  },
  changeLoadingState: ({ commit }, isLoading) => {
    commit('changeLoadingState', isLoading)
  },
  confirmationPass: ({ state }) => {},
  getUserInfo: ({ commit }) => {
    const token = localStorage.getItem('grw-token')
    if (token) {
      commit('changeLoadingState', true)
      fetch(`${apiGrowpany.baseURL}/usuario/info/?token=${token}`)
        .then(response => {
          return response.json()
        })
        .then(jsonData => {
          if (!jsonData.error) {
            commit('changeLoadingState', false)
            commit('setUser', jsonData.data)
            commit('MainModule/onLoginSuccess', true, { root: true })
          } else {
            throw Error
          }
        })
        .catch(_ => {
          commit('changeLoadingState', false)
          commit('MainModule/logout', null, { root: true })
        })
    }
  },
  handleFileUpload: ({ commit }, event) => {
    const file = event.target.files[0]
    if (file) {
      if (file.size <= 524288) {
        var reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = function() {
          commit(
            'onImageChanged',
            reader.result.substr(reader.result.indexOf(',') + 1)
          )
        }
        reader.onerror = function(error) {
          console.log('Error: ', error)
        }
        commit('saveUrlImage', URL.createObjectURL(file))
      }
    }
  },
  // inputs change
  generateUrlImage: ({ commit, state }, url) => {
    commit('generateUrlImage', url)
  },
  onNameChanged: ({ commit, state }, value) => {
    commit(
      'onNameChanged',
      value.target.value !== '' ? value.target.value : null
    )
  },
  onSurnameChanged: ({ commit, state }, value) => {
    commit(
      'onSurnameChanged',
      value.target.value !== '' ? value.target.value : null
    )
  },
  onEmailChanged: ({ commit, state }, value) => {
    commit(
      'onEmailChanged',
      value.target.value !== '' ? value.target.value : null
    )
  },
  onCellPhoneChanged: ({ commit, state }, value) => {
    commit(
      'onCellPhoneChanged',
      value.target.value !== '' ? value.target.value : null
    )
  },
  onPhoneChanged: ({ commit, state }, value) => {
    commit(
      'onPhoneChanged',
      value.target.value !== '' ? value.target.value : null
    )
  },
  onOldPasswordChanged: ({ commit, state, getters }, value) => {
    commit(
      'onOldPasswordChanged',
      value.target.value !== '' ? value.target.value : null
    )

    var myInit = {
      method: 'POST',
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify({
        pass: getters.md5OldPassword
      })
    }

    fetch(
      `https://www.growpany.com/growpanylab/api/oldpass/${state.user.id}`,
      myInit
    )
      .then(resp => resp.json())
      .then(data => {
        commit('setIsMatchOldPass', data.confirm)
      })
      .catch(e => {
        console.log(e)
      })
  },
  onNewPasswordChanged: ({ commit, state }, value) => {
    commit(
      'onNewPasswordChanged',
      value.target.value !== '' ? value.target.value : null
    )
  },
  onPasswordConfirmChanged: ({ commit, state }, value) => {
    commit(
      'onPasswordConfirmChanged',
      value.target.value !== '' ? value.target.value : null
    )
  },
  // Update
  update: ({ commit, state, getters }) => {
    const body = {
      token: localStorage.getItem('grw-token'),
      foto: state.formEdit.foto,
      nome: state.formEdit.nome,
      sobrenome: state.formEdit.sobrenome,
      email: state.formEdit.email,
      telefone: state.formEdit.telefone,
      celular: state.formEdit.celular,
      senha: getters.md5NewPassword
    }
    state.isLoading = true
    fetch(`${apiGrowpany.baseURL}/usuario/alterar`, {
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(result => {
        return result.json()
      })
      .then(jsonData => {
        if (!jsonData.error) {
          state.isLoading = false
          commit('onOldPasswordChanged', null)
          commit('onNewPasswordChanged', null)
          commit('setIsMatchOldPass', false)
          alert('Informações alteradas com sucesso')
        } else {
          throw new Error(jsonData.msg)
        }
      })
      .catch(_ => {
        state.isLoading = false
      })
  },
  // Reset state
  resetState: ({ commit }) => {
    commit('resetState')
  },
  resetFormState: ({ commit }) => {
    commit('resetFormState')
  }
}
