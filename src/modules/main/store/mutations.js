export default {
  navToLogin: state => { state.isPageLogin = true },
  navToRegister: state => { state.isPageLogin = false },
  navToBack: state => { state.isPageLogin = null },
  onLoginSuccess: (state, success) => {
    state.isLoged = success
  },
  logout: state => {
    for (const key in state.user) {
      state.user[key] = null
    }
  }

}
