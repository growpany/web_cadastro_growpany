export default {
  navToLogin: ({ commit }) => commit('navToLogin'),
  navToRegister: ({ commit }) => commit('navToRegister'),
  navToBack: ({ commit }) => commit('navToBack'),
  onLoginSuccess: ({ commit }, success) => commit('onLoginSuccess', success),
  logout: ({ commit }) => {
    commit('logout')
    commit('UserModule/onTokenChanged', null, { root: true })
    commit('onLoginSuccess', false)
    return true
  }
}
