import { getDefaultState } from './state'
export default {
  inputEmail: (state, value) => {
    state.auth.email = value
  },
  inputPassword: (state, value) => {
    state.auth.password = value
  },
  resetState: (state) => {
    Object.assign(state, getDefaultState())
  }
}
