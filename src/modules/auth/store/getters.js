import md5 from 'crypto-js/md5'
export default {
  getToken: (state) => {
    if (state.auth.email !== '' && state.auth.password !== '') {
      const hash = state.auth.email + (md5(state.auth.password).toString())
      return md5(hash).toString()
    }
    return null
  }
}
