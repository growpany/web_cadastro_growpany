const getDefaultState = () => {
  return {
    isLoading: false,
    auth: {
      email: '',
      password: ''
    }
  }
}
export { getDefaultState }
export default {
  isLoading: false,
  auth: {
    email: '',
    password: ''
  }
}
