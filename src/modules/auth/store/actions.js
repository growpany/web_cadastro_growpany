import apiGrowpany from '@/apiGrowpany'
export default {
  inputEmail: ({ commit }, input) => {
    commit('inputEmail', input.target.value)
  },
  inputPassword: ({ commit }, input) => {
    commit('inputPassword', input.target.value)
  },
  actionLogin: ({ commit, state, getters }) => {
    const body = {
      token: apiGrowpany.tokenApp,
      login: getters.getToken
    }
    state.isLoading = true
    return fetch(`${apiGrowpany.baseURL}/usuario/login/`, {
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(response => {
        return response.json()
      })
      .then(jsonData => {
        state.isLoading = false
        if (!jsonData.error) {
          commit('UserModule/setUser', jsonData.data, { root: true })
          return true
        }
      })
      .catch(() => {
        state.isLoading = false
        return false
      })
  },
  resetState: ({ commit }) => {
    commit('resetState')
  }
}
